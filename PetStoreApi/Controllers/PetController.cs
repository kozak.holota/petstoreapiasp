﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using PetStoreApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PetStoreApi.Controllers
{
    [Route("api/v3/pet")]
    public class PetController : Controller
    {
        [HttpPost]
        public ActionResult<Pet> CreatePet(Pet pet)
        {
            return new Pet
            {
                category = new Category { id = 11, name = "doggie" },
                name = "Buba",
                id = 12,
                photoUrls = new List<PhotoUrl>(),
                status = 0,
                tags = new List<Tag>()
            };
        }

        [HttpPut]
        public ActionResult<Pet> UpdatePet(Pet pet)
        {
            return new Pet {
                category = new Category { id = 11, name = "doggie" },
                name = "Buba",
                id = 12, photoUrls = new List<PhotoUrl>(),
                status=0,
                tags = new List<Tag>()
            };
        }

        [HttpGet("{id}")]
        public ActionResult<Pet> GetPetById(int id)
        {
            return new Pet
            {
                category = new Category { id = 11, name = "doggie" },
                name = "Buba",
                id = 12,
                photoUrls = new List<PhotoUrl>(),
                status = 0,
                tags = new List<Tag>()
            };
        }

        [HttpPost]
        public ActionResult<Pet> UpdatePet(int id, string name, string status)
        {
            return new Pet
            {
                category = new Category { id = 11, name = "doggie" },
                name = "Buba",
                id = 12,
                photoUrls = new List<PhotoUrl>(),
                status = 0,
                tags = new List<Tag>()
            };
        }

        [Route("findByStatus")]
        [HttpGet]
        public ActionResult<List<Pet>> GetPetsByStatus(string status)
        {
            return new List<Pet>();
        }

        [Route("findByTags")]
        [HttpGet]
        public ActionResult<List<Pet>> GetPetsByTags(List<string> tags)
        {
            return new List<Pet>();
        }
    }
}
