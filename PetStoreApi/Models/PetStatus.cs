﻿using System;
namespace PetStoreApi.Models
{
    public enum PetStatus
    {
        available,
        pending,
        sold
    }
}
