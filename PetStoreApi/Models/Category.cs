﻿using System;
namespace PetStoreApi.Models
{
    public class Category
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
