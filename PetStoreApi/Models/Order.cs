﻿using System;
namespace PetStoreApi.Models
{
    public class Order
    {
        public Guid id { get; init; }
        public int petId { get; set; }
        public int quantity { get; set; }
        public DateTimeOffset shipDate { get; init; }
        public OrderStatus status { get; set; }
        public bool complete { get; set; }
    }
}
