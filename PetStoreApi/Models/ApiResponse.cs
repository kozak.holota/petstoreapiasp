﻿using System;
namespace PetStoreApi.Models
{
    public class ApiResponse
    {
        public int code { get; set; }
        public string type { get; set; }
        public string message { get; set; }
    }
}
