﻿using System;
using System.Collections.Generic;

namespace PetStoreApi.Models
{
    public class Pet
    {
        public int id { get; init; }
        public string name { get; set; }
        public Category category { get; set; }
        public List<PhotoUrl> photoUrls { get; set; }
        public List<Tag> tags { get; set; }
        public PetStatus status { get; set; }

        public override string ToString()
        {
            return $"ID: {id}, Name: {name}, Category: {category}, Status: {status}";
        }
    }
}
