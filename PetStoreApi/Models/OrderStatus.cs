﻿using System;
namespace PetStoreApi.Models
{
    public enum OrderStatus
    {
        placed,
        approved,
        delivered
    }
}
