﻿using System;
namespace PetStoreApi.Models
{
    public class Customer
    {
        public int id { get; set; }
        public string username { get; set; }
        public Address address { get; set; }
    }
}
